Router.route("/", {
  name: "home"
});

Router.route("/dashboard", {
  name: "dashboard"
});

Router.route("/comment", {
  name: "comment"
});

Router.plugin("ensureSignedIn", {
  only: ["dashboard"]
});
