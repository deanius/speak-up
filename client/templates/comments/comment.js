
Template.comment.events({
  'click .comment-submit': preventDefault(function () {
    var rating = $(".comment-rating").val();
    var comment = $(".comment-comment").val();

    Meteor.call("Comments.insert", {
      rating: rating,
      comment: comment
    }, function (err, response) {
      console.log( err ? err : "Comment saved.");
    });
  })
})


function preventDefault(fn) {
  return function (e, template) {
    e.preventDefault();
    fn(e, template);
  };
}
