Meteor.startup(function() {

  Factory.define('comment', Comments, {
    comment: function() { return Fake.paragraph(_.random(1,3)); },
    rating: function() { return _.random(1, 5); }
  });

  if (Comments.find({}).count() === 0) {

    _(10).times(function(n) {
      Factory.create('comment');
    });

  }

});
